/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class ShapesSimulation {
    
   public static void main(String args[]){
       
    Circle c1 = new Circle();
    Square s1 = new Square();
    
    System.out.println(c1.getArea(8));
    System.out.println(c1.getPerimeter(8));
    System.out.println(s1.getArea(6));
    System.out.println(s1.getPerimeter(6));
       
   }
    
}
