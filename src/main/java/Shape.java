/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public abstract class Shape {
   
    
    
    
   public abstract double getArea(double d);
   
   public abstract double getPerimeter(double d);
     
    
}
