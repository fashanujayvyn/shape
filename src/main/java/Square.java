/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class Square extends Shape {
    
    
    
    public  double getArea(double d){
    
        return d * d;
        
    }
    public  double getPerimeter(double d){
        
     return d * 4;   
    }
    
}
