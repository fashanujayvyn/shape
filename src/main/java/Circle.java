/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class Circle extends Shape{
    
    
    public  double getArea(double d){
        return Math.PI * d *d;
        
    }
    public  double getPerimeter(double d){
        return 2 * Math.PI * d;
    }
    
}
